use std::io;
 
fn main(){
    let mut input = String::new();
    io::stdin().read_line(&mut input);
    let number:i32 = input.trim().parse().expect("this is not a number");
 
    if number%2==0&& number!=2{
        println!("YES");
    }
    else {
        println!("NO");
    }
}
