use std::io::{self, Write};
 
fn main() {
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Failed to read line");
    let mut n: usize = input.trim().parse().expect("this is not a number ");
 
    while n > 0 {
        //get an input string
        let mut s = String::new();
        io::stdin().read_line(&mut s).expect("this is not a string");
 
        let x = s.trim_end().len(); // Trim trailing whitespace, including newline characters
        let first = s.chars().next().unwrap();
        let last = s.chars().nth(x - 1).unwrap();
 
        if x > 10 {
            let minus = x - 2;
            println!("{}{}{}", first, minus, last);
        } else {
            print!("{}", s);
        }
        n -= 1;
    }
}

